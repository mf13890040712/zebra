package com.guosen.zebra.gateway.dao.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.guosen.zebra.core.common.ZebraConstants;
import com.guosen.zebra.core.grpc.anotation.ZebraConf;
import com.guosen.zebra.gateway.dto.ConfCenterRespDTO;
import com.guosen.zebra.gateway.dto.GatewayConf;
import com.guosen.zebra.gateway.route.model.RouteConfig;
import mockit.Expectations;
import mockit.Mocked;
import mockit.Tested;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class RouteConfigDaoImplTest {

    @Tested
    private RouteConfigDaoImpl routeConfigDaoImpl;

    @Mocked
    private OkHttpClient HTTP_CLIENT;

    @Mocked
    private ZebraConf ZEBRA_CONF;

    @Test
    public void testHttpReqError(@Mocked okhttp3.Call call) {

        new Expectations() {
            {
                HTTP_CLIENT.newCall((Request)any);
                result = call;
            }
            {
                try {
                    call.execute();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                result = new IOException("Mock exception");
            }
        };

        List<RouteConfig> routeConfigs = routeConfigDaoImpl.getConfigs();

        assertThat(routeConfigs, is(empty()));
    }

    @Test
    public void testHttpRespBodyNull(@Mocked okhttp3.Call call, @Mocked Response response) {

        new Expectations() {
            {
                HTTP_CLIENT.newCall((Request)any);
                result = call;
            }
            {
                try {
                    call.execute();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                result = response;
            }
            {
                response.body();
                result = null;
            }
        };

        List<RouteConfig> routeConfigs = routeConfigDaoImpl.getConfigs();

        assertThat(routeConfigs, is(empty()));
    }

    @Test
    public void testResultFail(@Mocked okhttp3.Call call,
                               @Mocked Response response,
                               @Mocked ResponseBody responseBody) {
        new Expectations() {
            {
                HTTP_CLIENT.newCall((Request)any);
                result = call;
            }
            {
                try {
                    call.execute();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                result = response;
            }
            {
                response.body();
                result = responseBody;
            }
            {
                ConfCenterRespDTO confCenterRespDTO = new ConfCenterRespDTO();
                confCenterRespDTO.setCode(ZebraConstants.FAIL);

                try {
                    responseBody.string();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                result = JSONObject.toJSONString(confCenterRespDTO);
            }
        };

        List<RouteConfig> routeConfigs = routeConfigDaoImpl.getConfigs();

        assertThat(routeConfigs, is(empty()));
    }

    @Test
    public void testResultSuccessButEmpty(@Mocked okhttp3.Call call,
                               @Mocked Response response,
                               @Mocked ResponseBody responseBody) {
        new Expectations() {
            {
                HTTP_CLIENT.newCall((Request)any);
                result = call;
            }
            {
                try {
                    call.execute();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                result = response;
            }
            {
                response.body();
                result = responseBody;
            }
            {
                ConfCenterRespDTO confCenterRespDTO = new ConfCenterRespDTO();
                confCenterRespDTO.setCode(ZebraConstants.SUCCESS);

                try {
                    responseBody.string();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                result = JSONObject.toJSONString(confCenterRespDTO);
            }
        };

        List<RouteConfig> routeConfigs = routeConfigDaoImpl.getConfigs();

        assertThat(routeConfigs, is(empty()));
    }

    @Test
    public void testResultSuccess(@Mocked okhttp3.Call call,
                                          @Mocked Response response,
                                          @Mocked ResponseBody responseBody) {
        new Expectations() {
            {
                HTTP_CLIENT.newCall((Request)any);
                result = call;
            }
            {
                try {
                    call.execute();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                result = response;
            }
            {
                response.body();
                result = responseBody;
            }
            {
                GatewayConf gatewayConf = new GatewayConf();
                gatewayConf.setService("com.guosen.zebra.sample.FirstService");
                gatewayConf.setVersion("1.0.0");
                gatewayConf.setPath("/hello/world");
                gatewayConf.setGroup("set");
                gatewayConf.setText("/sayHi->sayHi\n/sayHello->sayHello");

                List<GatewayConf> gatewayConfigs = new ArrayList<>();
                gatewayConfigs.add(gatewayConf);

                ConfCenterRespDTO confCenterRespDTO = new ConfCenterRespDTO();
                confCenterRespDTO.setCode(ZebraConstants.SUCCESS);
                confCenterRespDTO.setData(JSONArray.toJSON(gatewayConfigs));
                try {
                    responseBody.string();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                result = JSONObject.toJSONString(confCenterRespDTO);
            }
        };

        List<RouteConfig> routeConfigs = routeConfigDaoImpl.getConfigs();

        assertThat(routeConfigs, is(not(empty())));
        assertThat(routeConfigs.size(), is(1));

        RouteConfig routeConfig = routeConfigs.get(0);

        assertThat(routeConfig.getServiceName(), is("com.guosen.zebra.sample.FirstService"));
        assertThat(routeConfig.getVersion(), is("1.0.0"));
        assertThat(routeConfig.getUrlPrefix(), is("/hello/world"));
        assertThat(routeConfig.getGroup(), is("set"));
        assertThat(routeConfig.getMethodMappings(), is("/sayHi->sayHi\n/sayHello->sayHello"));

    }

}
