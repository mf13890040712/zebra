package com.guosen.zebra.gateway.route.cache;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 路由缓存刷新定时器<br>
 *     使用定期任务刷新路由缓存。
 *     其实最好是使用MQ，配置中心修改发送一个MQ，然后网关接收。
 *     但是这样Zebra就又和MQ耦合了，所以开源版本使用定时器刷新方式。
 *     有需要的修改可以配置中心和网关，使用MQ来更新路由缓存，实时性和性能会更好。
 */
@Configuration
@EnableScheduling
@Component
public class RouteCacheRefreshTimer {

    @Autowired
    private RouterCacheRefresher routerCacheRefresher;

    // 每两分钟刷新一下
    @Scheduled(initialDelay=60 * 2 * 1000, fixedRate=60 * 2 * 1000)
    public void refresh() {
        routerCacheRefresher.refresh();
    }
}
