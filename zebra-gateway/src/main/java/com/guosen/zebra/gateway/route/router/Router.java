package com.guosen.zebra.gateway.route.router;

import com.guosen.zebra.gateway.route.model.RouteInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * API 网关路由器
 */
@Component
public class Router {

    @Autowired
    private PreciseRouter preciseRouter;

    @Autowired
    private FuzzyRouter fuzzyRouter;

    /**
     * 路由
     * @param requestURI    请求 URI
     * @return  路由信息
     */
    public RouteInfo route(String requestURI) {

        // 先根据整 URL 做精确匹配
        RouteInfo preciseRouteInfo = preciseRouter.route(requestURI);
        if (preciseRouteInfo != null) {
            return preciseRouteInfo;
        }

        // 找不到的话，根据前缀做模糊匹配路由
        return fuzzyRouter.route(requestURI);
    }
}
