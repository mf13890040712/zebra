package com.guosen.zebra.maven.plugin;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;

public final class CommonUtils {

    private CommonUtils(){

    }

    public static String findPojoTypeFromCache(String sourceType, Map<String, String> pojoTypes) {
        if(sourceType.equals(".com.guosen.zebra.dto.RetMessage")){
            return "com.guosen.zebra.dto.commondto.RetMessage";
        }else if(sourceType.equals(".com.guosen.zebra.dto.ResultDTO")){
            return "com.guosen.zebra.dto.commondto.ResultDTO";
        }else if(sourceType.equals(".com.guosen.zebra.dto.ReturnDTO")){
            return "com.guosen.zebra.dto.commondto.ResultDTO";
        }
        String type = StringUtils.substring(sourceType, StringUtils.lastIndexOf(sourceType, ".") + 1);
        String pojoType = pojoTypes.get(type);
        if (StringUtils.isNotEmpty(pojoType)) {
            return pojoType;
        }

        // 支持业务自定义公共PB，但是业务公共PB得符合我们的特殊规范
        return forCustomizedCommon(sourceType);
    }

    public static String findNotIncludePackageType(String sourceType) {
        String type = StringUtils.substring(sourceType, StringUtils.lastIndexOf(sourceType, ".") + 1);
        return type;
    }

    private static String forCustomizedCommon(String sourceType) {
        String sourceTypeWithoutPrefix = StringUtils.removeStart(sourceType, ".");
        String packageName = StringUtils.substringBeforeLast(sourceTypeWithoutPrefix, ".");
        String clzName = StringUtils.substringAfterLast(sourceTypeWithoutPrefix, ".");

        return packageName + ".commonmodel." + clzName;
    }
}
